<?php
/**
 * Created by PhpStorm.
 * User: Priscila Martins
 * Date: 02/01/2020
 * Time: 19:14
 */
function formatterValue($row)
{
    preg_match("((:?\d+.?)\d+)", $row, $matches);
    $number = $matches[0];
    $number =str_replace('.', '', $number);
    $teste = number_format($number,2,',', '.');
    return $teste;
}

try {

    $s1 = "Melhor preço sem escalas R$ 1.367(1)";
    echo formatterValue($s1) . PHP_EOL;
    $s2 = "Melhor preço com escalas R$ 994 (1)";
    echo formatterValue($s2) . PHP_EOL;

}
catch (Exception $e) {
    echo $e->msg();
}