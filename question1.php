<?php

/**
 * Created by PhpStorm.
 * User: Priscila Martins
 * Date: 02/01/2020
 * Time: 19:14
 */

function bhaskara($a, $b, $c)
{
    $delta = ($b * $b) - (4 * $a * $c);
    if ($delta < 0) {
        return "Delta negativo, não possui raízes.";
    } else {
        $rDelta = sqrt($delta);
        if ($delta == 0) {
            $r1 = -$b/(2*$a);
            return "Delta igual a 0, temos somente uma raiz, sendo ela: " . $r1;
        } else {
            $r1 = (-$b + sqrt($delta))/ (2*$a);
            $r2 = (-$b - sqrt($delta))/ (2*$a);
            return "Delta maior que 0, temos r1: " . $r1 .  " e temos r2: " . $r2;
        }

    }

}

try {

    //TESTE 1
    $a1 = 5;
    $b1 = 1;
    $c1 = 6;
    echo bhaskara($a1, $b1, $c1) . PHP_EOL;

    //TESTE 2
    $a1 = 4;
    $b1 = -4;
    $c1 = 1;
    echo bhaskara($a1, $b1, $c1) . PHP_EOL;

    //TESTE 3
    $a1 = 1;
    $b1 = -1;
    $c1 = -30;
    echo bhaskara($a1, $b1, $c1) . PHP_EOL;

}
catch (Exception $e) {
    echo $e->msg();
}