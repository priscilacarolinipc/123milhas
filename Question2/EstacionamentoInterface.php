<?php

/**
 * Created by PhpStorm.
 * User: PRIETHATY
 * Date: 02/01/2020
 * Time: 21:17
 */
interface EstacionamentoInterface
{
    public function estacionar(VeiculoInterface $veiculo);
    public function qtdVagas();
}