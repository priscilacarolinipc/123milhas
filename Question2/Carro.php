<?php
/**
 * Created by PhpStorm.
 * User: PRIETHATY
 * Date: 02/01/2020
 * Time: 21:12
 */

class Carro implements VeiculoInterface {

    private $marca;
    private $placa;
    private $status;

    public function setMarca(string $marca)
    {
        $this->marca = $marca;
    }

    public function getMarca(): string
    {
        return $this->marca;
    }

    public function setPlaca(string $placa)
    {
        $this->placa = $placa;
    }

    public function getPlaca(): string
    {
        return $this->placa;
    }

    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}