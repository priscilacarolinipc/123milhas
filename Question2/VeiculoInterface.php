<?php

/**
 * Created by PhpStorm.
 * User: PRIETHATY
 * Date: 02/01/2020
 * Time: 21:04
 */
interface VeiculoInterface
{
    public function setMarca(string $marca);
    public function getMarca(): string;
    public function setPlaca(string $placa);
    public function getPlaca(): string;
    public function setStatus(string $status);
    public function getStatus(): string;


}