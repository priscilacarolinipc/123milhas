<?php

/**
 * Created by PhpStorm.
 * User: PRIETHATY
 * Date: 02/01/2020
 * Time: 21:19
 */
class Estacionamento implements EstacionamentoInterface
{

    private $listaVeiculos =  array();
    private $capacidadeEstacionamento = 100;

    public function estacionar(VeiculoInterface $veiculo)
    {
        if($this->qtdVagas() > 0) {
            array_push($this->listaVeiculos, $veiculo);
            $veiculo->setStatus('Estacionado');
            return "Veículo Estacionado";
        }else{
            return "Estacionamento Lotado";
        }
    }

    public function qtdVagas()
    {
        return $this->capacidadeEstacionamento -  count($this->listaVeiculos);
    }
}